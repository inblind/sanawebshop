﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using WebShop.Sana.Model;

namespace WebShop.Sana.DataAccess
{
    public class WebShopDBContext : DbContext
    {

        public WebShopDBContext() : base("name=WebShopSana")
        { }

        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<ProductCategory> ProductCategory { get; set; }
    }
}
