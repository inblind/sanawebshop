﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebShop.Sana.Model;

namespace WebShop.Sana.DataAccess
{
    public class Repository
    {
        WebShopDBContext dbContext = new WebShopDBContext();

        public List<Product> GetAllProducts() {
            return dbContext.Product.ToList();
        }

        public List<ProductCategory> GetAllProductCategories()
        {
            return dbContext.ProductCategory.ToList();
        }

        public List<Category> GetAllCategories()
        {
            return dbContext.Category.ToList();
        }

        public Product SaveProduct(Product p)
        {
            dbContext.Product.Add(p);

            dbContext.SaveChanges();
            return p;
        }

        public ProductCategory SaveProductCategory(ProductCategory pc)
        {
            dbContext.ProductCategory.Add(pc);
            dbContext.SaveChanges();
            return pc;
        }

        public Category SaveCategory(Category c)
        {
            dbContext.Category.Add(c);
            dbContext.SaveChanges();
            return c;
        }

    }
}
