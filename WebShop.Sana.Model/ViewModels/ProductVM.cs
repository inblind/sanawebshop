﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebShop.Sana.Model
{
    public class ProductVM
    {

        public ProductVM()
        {
            SelectedCategories = new List<CategoryVM>();
        }
        public int? Id { get; set; }
        public string Title { get; set; }
        public string SKU { get; set; }
        public double Price { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public List<CategoryVM> SelectedCategories { get; set; }
        public int SelectedCategoryId { get; set; }

        public List<CategoryVM> Categories { get; set; }
    }
}