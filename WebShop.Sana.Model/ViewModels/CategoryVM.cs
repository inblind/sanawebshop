﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebShop.Sana.Model
{
    public class CategoryVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}