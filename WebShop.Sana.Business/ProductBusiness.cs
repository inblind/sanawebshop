﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebShop.Sana.DataAccess;
using WebShop.Sana.Model;

namespace WebShop.Sana.Business
{
    public class ProductBusiness
    {
        List<Category> categories = null;
        Repository repo = new Repository();

        public ProductBusiness()
        {
            categories = repo.GetAllCategories();
        }
        public List<ProductVM> GetAllProducts() {

            List<ProductVM> productVM = new List<ProductVM>();

            var products = repo.GetAllProducts();
            categories = repo.GetAllCategories();

            if (!categories.Any())
                AddDefaultCategories();

            foreach (var item in products)
            {
                productVM.Add(MapToProductVM(item));
            }

            return productVM;
        }

        public List<CategoryVM> GetAllCategories()
        {
            List<CategoryVM> categoriesVM = new List<CategoryVM>();

            foreach (var item in categories)
            {
                CategoryVM categoryVM = new CategoryVM()
                {
                    Id = item.Id,
                    Description = item.Description,
                    Name = item.Name
                };

                categoriesVM.Add(categoryVM);
            }

            return categoriesVM;
        }

        private void AddDefaultCategories()
        {
            repo.SaveCategory(new Category { Name="Tech", Description="All tech related", CreatedDate=DateTime.Now, UpdatedDate = DateTime.Now});
            repo.SaveCategory(new Category { Name = "Sport", Description = "All Sport related", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now });
            repo.SaveCategory(new Category { Name = "Beauity", Description = "All Beauity related", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now });
            repo.SaveCategory(new Category { Name = "Gaming", Description = "All Gaming related", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now });
            repo.SaveCategory(new Category { Name = "Cars", Description = "All Cars related", CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now });
        }

        public ProductVM SaveProduct(ProductVM productVM, bool storeDatabase = true)
        {
            if (productVM.Id != null)
                return null;

            Product p = new Product()
            {
                Title = productVM.Title,
                SKU = productVM.SKU,
                Price = productVM.Price,
                CreatedDate = DateTime.Now,
                UpdatedDate = DateTime.Now,
            };

            if (storeDatabase)
                repo.SaveProduct(p);

            if (productVM?.Categories != null)
            {
                foreach (var cat in productVM?.Categories)
                {
                    ProductCategory pc = new ProductCategory()
                    {
                        ProductId = p.Id,
                        CategoryId = cat.Id,
                        CreatedDate = DateTime.Now,
                        UpdatedDate = DateTime.Now
                    };
                    if (storeDatabase)
                        repo.SaveProductCategory(pc);
                }
            }

            if (productVM.SelectedCategoryId != 0)
            {
                ProductCategory pc = new ProductCategory()
                {
                    ProductId = p.Id,
                    CategoryId = productVM.SelectedCategoryId,
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now
                };
                if (storeDatabase)
                    repo.SaveProductCategory(pc);
            }

            productVM.Id = p.Id;

            return productVM;
        }

        private ProductVM MapToProductVM(Product product) {

            ProductVM p = null;

            if (product != null)
            {
                p = new ProductVM
                {
                    Id = product.Id,
                    Title = product.Title,
                    SKU = product.SKU,
                    Price = product.Price,
                    CreatedDate = product.CreatedDate,
                    UpdatedDate = product.UpdatedDate,
                };

                p.Categories = MapToCategoriesVM(product.ProductCategories?.ToList());
            }

            return p;

        }

        private List<CategoryVM> MapToCategoriesVM(List<ProductCategory> productCategories) {
            List<CategoryVM> categoriesVM = null;

            if (categories.Any())
            {
                categoriesVM = new List<CategoryVM>();

                foreach (var c in categories)
                {
                    var pvc = categories.FirstOrDefault(a => a.Id == c.Id);

                    CategoryVM categoryVM = new CategoryVM();
                    categoryVM.Id = c.Id;
                    categoryVM.Description = pvc?.Description;
                    categoryVM.Name = pvc?.Name;

                    categoriesVM.Add(categoryVM);
                }
            }

            return categoriesVM;
        }


    }
}
