﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebShop.Sana.Business;
using WebShop.Sana.Model;

namespace WebShop.Sana.Web.Controllers
{
    public class ProductController : Controller
    {

        ProductBusiness business = new ProductBusiness();
        public ActionResult Index()
        {
            IEnumerable<ProductVM> productVMList = null;
            if (!Session["DatabaseMode"].Equals("Active") && (Session["ProductViewModels"] as List<ProductVM>).Any())
            {
                productVMList = Session["ProductViewModels"] as List<ProductVM>;
            }
            else
            {
                productVMList = business.GetAllProducts();
                Session["ProductViewModels"] = productVMList;
            }

            return View(productVMList);
        }

        public ActionResult Create()
        {
            ProductVM productVM = new ProductVM();
            productVM.Categories = business.GetAllCategories();
            return View(productVM);
        }

        [HttpPost]
        public ActionResult Create(ProductVM productVM)
        {

            List<ProductVM> productsViewModels = null;
            if (ModelState.IsValid)
            {
                if (!Session["DatabaseMode"].Equals("Active"))
                {
                    productsViewModels =  Session["ProductViewModels"] as List<ProductVM>;
                    productsViewModels.Add(productVM);

                    Session["ProductViewModels"] = productsViewModels;
                }
                else
                {
                    business.SaveProduct(productVM);
                    productsViewModels = business.GetAllProducts();
                }
            }

            return View("Index", productsViewModels);
        }
    }
}
